# Projet MAVEN

Ce projet offre un plugin Maven ainsi qu’une application Spring-Boot qui renvoie la valeur de la suite de Fibonacci au rang de l’entier que l’on passe.
Il est formé d’un parent et trois enfants:

- Un **core**
- Un **plugin**
- Une **app Spring-Boot**

## POM.xml
 **POM parent**

Il contient les modules enfants :
```
<modules>
        <module>core</module>
        <module>plugin</module>
        <module>spring-app</module>
</modules>
```
-Pour les modules enfants, on rajoute les dépendances et build nécessaires. Par exemple, dans cette section nous avons utilisé le groupId et la version pour définir la valeur de l'élément core avec la syntaxe ${project.groupId} et ${project.artifactId} => C’est des références à des propriétés.

-La balise dependencyManagement dans un POM parent nous permet de référencer une dépendance dans un projet fils sans avoir à spécifier la version.
Maven va parcourir la hiérarchie des POMs et quand il trouve une balise dependencyManagement, il utilisera la version déclarée dans cette dernière.
Quand Maven hérite de dépendances, il va ajouter les dépendances définies dans les projets fils à celles des projets parents.


 **POM enfants**
 
Les versions des dépendances des composants enfants  sont définis dans le POM parent.

- Les goals utilisés sont:

    **compile** : Compile les sources 
    
    **test** : Lance les tests unitaires 
    
    **verify** : Lance des tests de validation du package créé. 
    
    **package**: Prépare la distribution du projet. (archives Jar) 

                
Pour lancer un goal Maven sur un projet, il faut se placer à la racine du projet et exécuter la commande :   
``` 
    mvn <nom du goal>
```                   

Le cycle clean, permet de nettoyer le répertoire de travail de Maven. Il fait référence au goal clean du plugin clean :

```
    mvn clean
```
Pour génèrer le site du documentation du projet il faut faire :
```
    mvn site verify
```

## Scope de dépendance

- #### compile

C'est le scope par défaut ; toutes les dépendances sont dans ce scope compile si aucun scope n'est précisé.

- #### provided

Les dépendances du scope provided sont utilisées lorsque le JDK doit fournir du jar.

- #### test

Les dépendances du scope test servent durant les phases de compilation et d'exécution des tests. 


L’IDE utilisé est le Intellij Idea

## Difficultés

- La configuration des POM et la gestion des dépendances, m'a pris du temps (débogage de la duplication de dépendance et la compréhension).
- La création du site et son déploiement.
- Le projet m'a pris plus que 5 heures.

# Partie projet Intégration continue

Cette partie du projet, concerne la mise en oeuvre de l'intégration continue sur le projet java du tp maven.

Au début, j’ai déposé mon projet sur github et j’ai commencé à travailler avec Travis CI intégré sur Github.
Mais en comparant avec l’outil d’intégration de GitLab, j’ai trouvé que celui de GitLab c’est plus pratique. 
Donc, j’ai importé mon projet maven sur GitLab.

Avec l’activation de l’outil de l’intégration continue, GitLab génère automatiquement un fichier par défaut appelé .gitlab-ci.yml.
En configurant ce fichier, on crée notre Pipeline.

Gitlab génère du template pour faciliter la configuration du gitlab-ci.yml.
En utilisant ce template on crée plus facilement le fichier de configuration.

Nous avons configuré l’intégration en 3  stages
- construire,
- tester
- déployer

Pour builder avec JavaJdk8

```
   .compile: &compile
      stage: build
      script:
        - 'mvn $MAVEN_CLI_OPTS test-compile site'

    # Validate merge requests using JDK8
    compile:jdk8:
      <<: *compile
      image: maven:3.3.9-jdk-8

```
***.validate*** peut être vue comme un ***#define*** qu'on recopie.
On crée donc une app avec site.

Pour les tests, nous les faisons sur 2 versions majeures et différentes de java en paralléle :

```
    .verify: &verify
      stage: test
      script:
        - 'mvn $MAVEN_CLI_OPTS verify'
      except:
        - master
        
    verify:jdk11:
      <<: *verify
      image: maven:3.6.0-jdk-11
```

Déployer seulement par le master et une seule version de jdk qui l'utilise (pour le mvn site) :

```
    site:jdk8:
    # Use stage test here, so the pages job may later pickup the created site.
    stage: build
    script:
        - 'mvn $MAVEN_CLI_OPTS site site:stage'
    only:
        - master
    # Archive up the built documentation site.
    artifacts:
        paths:
            - target/staging/
    image: maven:3.3.9-jdk-8

```

## Merge Request
L'intérêt du merge request est faire en sorte que si le test est bien passé dans une branche autre que la master, 
Pour tester :
- J'ai configuré le ***.gitlab-ci.yml*** en donnant la permission d'effectuer le *stage* *test* aux branches crées **sauf la branche master**, car le test est otionnel dans le cycle de vie du projet.
```
    .verify: &verify
      stage: test
      script:
        - 'mvn $MAVEN_CLI_OPTS verify'
      except:
        - master
        
    verify:jdk11:
      <<: *verify
      image: maven:3.6.0-jdk-11
```
- J'ai créée une branche ***test0***.
- J'ai déposé le projet sur la branche *test0* avec :
```
    git push origin test0
```
- J'ai efffectué une modification dans le code de la class ***TestFibonacciCore.java*** en ajoutant un commentaire. Puis, j'ai fait git push.
- Automatiquement le gitlab lance la pipeline sur la branche *test0*.
- Ensuite, j'ai fait le demande merge sur gitlab (manuelle).
- Acceptation du merge avec le maintainer [moi :) ]
- Vu que tout va bien, la pipeline se lance automatiquement sur la branche *master* 

## Difficultés

- Le projet m'a pris plus que 5 heures.
- La prise ne main d'un nouveau outil et surtout la compréhension et la configuration m'ont pris du temps.
- La création du site et les liaisons se passent bien sauf que sur ***Settings*** -> ***Pages***, le site ne s'affiche pas.
  J'ai essayé de chercher la solution, ils disaient qu'il faut attendre et rafraîchir et j'ai essayé mais ça ne marche pas et par contre ça a marché pour d'autres personnes et je n'arrive pas à comprendre pourquoi.En local le site marche.
