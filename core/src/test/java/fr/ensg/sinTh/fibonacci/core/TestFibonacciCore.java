package fr.ensg.sinTh.fibonacci.core;

import junit.framework.TestCase;
import org.junit.Test;

import static java.lang.Long.MIN_VALUE;

public class TestFibonacciCore extends TestCase {



    @Test
    public void testFibo0() {
        /////comment to test Merge
        assertEquals(0, FibonacciCore.fibonacci(0, 0, 1));
    }

    @Test
    public void testFibo1() {

        assertEquals(1, FibonacciCore.fibonacci(1, 0, 1));
    }
    @Test
    public void testFiboMoin() {

        assertEquals(0, FibonacciCore.fibonacci(-1, 0, 1));
    }
    @Test
    public void testFibo() {

        assertEquals(0, FibonacciCore.fibonacci(MIN_VALUE, 0, 1));
    }

   /* @Test
    public void testFibonacci() {

        assertEquals(233, FibonacciCore.fibonacci(9, 0, 1));
    }*/

    @Test
    public void testFibona() {
        assertEquals(233, FibonacciCore.fibonacci(13, 0, 1));
    }



}