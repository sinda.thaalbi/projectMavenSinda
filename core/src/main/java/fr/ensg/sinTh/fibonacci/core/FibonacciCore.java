package fr.ensg.sinTh.fibonacci.core;

public class FibonacciCore {

    /**
     * @param n
     * @param a
     * @param b
     * @return
     */
    // fonction qui calcule F(n) par recursion
    public static long fibonacci(long n, long a, long b) {

        return (n > 0) ? fibonacci(n - 1, b, a + b) : a;
    }
}
